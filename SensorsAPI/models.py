from django.db import models
from django.utils.translation import gettext as _

class Sensors(models.Model):
    name = models.CharField(verbose_name=_('Name'), max_length=100)
    desc = models.CharField(verbose_name=_('Description'), max_length=255)
    lat = models.DecimalField(verbose_name=_('Latitude'), max_digits=9, decimal_places=6)
    lon = models.DecimalField(verbose_name=_('Longitude'), max_digits=9, decimal_places=6)


class AirData(models.Model):
    sensor = models.ForeignKey('Sensors', verbose_name=_('Sensor'), on_delete=models.CASCADE)
    pm1_0 = models.IntegerField(verbose_name=_('Particulates (<1.0 micrometer)'))
    pm2_5 = models.IntegerField(verbose_name=_('Particulates (<2.5 micrometer)'))
    airq = models.IntegerField(verbose_name=_('Air quality'))
    humidity = models.IntegerField(verbose_name=_('Humidity'))
    temp = models.DecimalField(max_digits=3, decimal_places=1)
    time = models.DateTimeField(auto_now_add=True, verbose_name=_('Record date/time'))
