from .models import Sensors, AirData
from rest_framework import serializers


class SensorsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sensors
        fields = '__all__'

class AirDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = AirData
        fields = '__all__'
