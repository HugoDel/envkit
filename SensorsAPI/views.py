from rest_framework import viewsets

from SensorsAPI.models import Sensors, AirData
from .serializers import SensorsSerializer, AirDataSerializer


class SensorsViewSet(viewsets.ModelViewSet):
    """
    API endpoint to list the deployed sensors
    """
    queryset = Sensors.objects.all()
    serializer_class = SensorsSerializer


class AirDataViewSet(viewsets.ModelViewSet):
    """
    API endpoint to list the recorded air data
    """
    queryset = AirData.objects.all()
    serializer_class = AirDataSerializer
