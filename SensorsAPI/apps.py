from django.apps import AppConfig


class SensorsapiConfig(AppConfig):
    name = 'SensorsAPI'
